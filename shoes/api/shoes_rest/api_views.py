from django.shortcuts import render
from common.json import ModelEncoder
from django.http import JsonResponse
import json
from django.views.decorators.http import require_http_methods
from .models import Shoes, BinVo

class BinVoEncoder(ModelEncoder):
    model = BinVo
    properties=[
    "import_href", 
    "closet_name", 
    "bin_number", 
    "bin_size" 
    ]

class ListShoesEncoder(ModelEncoder):
    model = Shoes
    properties = [
        "manufacturer",
        "model_name",
        "color_name",
        "picture_url",
        "bin",
        "id",
    ]
    encoders = {
        "bin": BinVoEncoder(),
    }

require_http_methods(["GET", "POST"])
def api_list_shoes(request, BinVo_id=None):
    if request.method == "GET":
        shoes = Shoes.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ListShoesEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            bin_href = content["bin"]
            bin = BinVo.objects.get(import_href=bin_href)
            content["bin"] = bin
        except BinVo.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin id"},
                status=400,
            )
        
        newBin = Shoes.objects.create(**content)
        return JsonResponse(
            newBin,
            encoder=ListShoesEncoder,
            safe=False,
        )

@require_http_methods(["GET","DELETE"])
def api_show_shoes(request, id=None):
    if request.method == "GET":
        location = Shoes.objects.get(id=id)
        return JsonResponse(
            location,
            encoder=ListShoesEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Shoes.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})