from django.db import models
from django.urls import reverse
class BinVo(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    closet_name = models.CharField(max_length=100, default=None)
    bin_number = models.PositiveSmallIntegerField(default=None)
    bin_size = models.PositiveSmallIntegerField(default=None)

    def __str__(self):
        return self.closet_name
    
class Shoes(models.Model):
    manufacturer=models.CharField(max_length=200, default=None)
    model_name=models.CharField(max_length=200, default=None)
    color_name=models.CharField(max_length=200, default=None)
    picture_url=models.URLField( max_length=200, default=None)


    bin = models.ForeignKey(
        BinVo,
        related_name="bin",
        on_delete=models.CASCADE,
        default=None
    )

    def get_api_url(self):
        return reverse("api_show_shoes", kwargs={"pk": self.pk})
    
    def __str__(self):
        return self.name

  
    