from django.shortcuts import render
from common.json import ModelEncoder
from django.http import JsonResponse
import json
from django.views.decorators.http import require_http_methods
from .models import Hats, LocationVO

class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "import_href",
        "closet_name",
        "section_number",
        "shelf_number"
        ]

class ListHatsEncoder(ModelEncoder):
    model = Hats
    properties = [
        "styleName",
        "color",
        "pictureUrl",
        "fabric",
        "location",
        "id",
    ]
    encoders = {
        "location": LocationVOEncoder(),
    }

# Create your views here.
@require_http_methods(["GET", "POST"])
def api_list_hats(request, id=None):
    if request.method == "GET":
        hats = Hats.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=ListHatsEncoder,
        )
    else:
        content = json.loads(request.body)
        print(content)
        try:
            location_href = content["location"]
            location = LocationVO.objects.get(import_href=location_href)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )

        newlocation = Hats.objects.create(**content)
        return JsonResponse(
            newlocation,
            encoder=ListHatsEncoder,
            safe=False,
        )


@require_http_methods(["GET", "DELETE"])
def api_show_hats(request, id=None):
    if request.method == "GET":
        location = Hats.objects.get(id=id)
        return JsonResponse(
            location,
            encoder=ListHatsEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Hats.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
