from django.contrib import admin
from .models import Hats

# Register your models here.
@admin.register(Hats)
class HatsAdmin(admin.ModelAdmin):
    list_display = (
        'styleName', 'pictureUrl', 'fabric', 'color', 'location'
        )
