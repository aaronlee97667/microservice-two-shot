from django.db import models
from django.urls import reverse

# Create your models here.

class LocationVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    closet_name = models.CharField(max_length=100)
    section_number = models.PositiveSmallIntegerField()
    shelf_number = models.PositiveSmallIntegerField()

    def __str__(self):
        return self.closet_name


class Hats(models.Model):
    styleName = models.CharField(max_length=200, default=None)
    pictureUrl = models.CharField(max_length=200, default=None, null=True)
    fabric = models.CharField(max_length=200, default=None)
    color = models.CharField(max_length=200)

    location = models.ForeignKey(
        LocationVO,
        related_name="location",
        on_delete=models.CASCADE,
        default=None
    )

    def get_api_url(self):
        return reverse("api_show_hats", kwargs={"pk": self.pk})

    def __str__(self):
        return self.name
