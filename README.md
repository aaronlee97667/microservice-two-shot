# Wardrobify

Team:

* Aaron Lee - Hats Microservice
* Diganta Roy - Shoes Microservice

## Design
Made Wardrobify's main page contain two buttons that link to lists displaying either shoes or hats.
Within the lists, users may click on locate to find the respective bin/location of their shoes/hat.
The lists will also display all the data for the shoes/hats, and contains a delete button.

Users can add hats or shoes by navigating with Nav bar to the respective "Add a (shoe/hat)" and then
filling out the form.  User must input a valid image address for it to be displayed on the list.

Before use, the database for wardrobe must be populated so that the add function can associate a location/bin.

## Shoes microservice

I have a shoe model, that recieves the manufacturer, model name, color name, and the associated picture url for the shoe product that is being stored into the bin. The BinVo is responsible for communicating with the Bin Model within the Wardrobe Api. Therefore allowing for both APIs to communicate, throught the poller.

## Hats microservice

The Hats microservice displays the style, color, picture, and fabric material of the collection of hats.
The service allows hats to be made and placed into containers with reference to the location value object that was
made by polling.  This allows communication between the wardrobe api and the hats microservice.
