import { useState, useEffect } from 'react'
import { Link } from 'react-router-dom'

function ShoeList () {
    const [shoes, setShoes] = useState([])

    const getData = async () => {
        const response = await fetch('http://localhost:8080/api/shoes/')
        if (response.ok) {
            const data = await response.json()
            setShoes(data.shoes)
        }
    }

    useEffect(()=> {
        getData()
    }, [])
    const handleClick = async (e) => {
        console.log(e.target)
        const shoeresponse = await fetch(`http://localhost:8080/api/shoes/${e.target.id}/`)
        if (shoeresponse.ok){
            const shoedetails = await shoeresponse.json()
            console.log(shoedetails)
            alert(`The ${shoedetails.manufacturer} ${shoedetails.model_name} can be found in: \n\nCloset Name: ${shoedetails.bin.closet_name}\nBin Number: ${shoedetails.bin.bin_number}\nBin Size: ${shoedetails.bin.bin_size}`
            )
        }
    }
    const handleDelete = async (e) => {
        console.log(e.target.id)
        const shoesUrl = `http://localhost:8080/api/shoes/${e.target.id}/`

        const fetchConfigs = {
            method: "Delete",
            headers: {
                "Content-Type": "application/json"
            }
        }

        const response = await fetch(shoesUrl, fetchConfigs)
        getData()
    }

    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Manufacturer</th>
                    <th>Model</th>
                    <th>Color</th>
                    <th>Image</th>
                </tr>
            </thead>
            <tbody >
                {shoes.map(shoe => {
                return (
                    <tr key={shoe.id}>
                        <td>{ shoe.manufacturer }</td>
                        <td>{ shoe.model_name }</td>
                        <td>{ shoe.color_name }</td>
                        <td><img alt="" style={{ width: 200, height: 200}} src={shoe.picture_url}/></td>
                        <td><button onClick={handleClick} value={shoe.bin} id={shoe.id} className="btn btn-primary">Locate</button></td>
                        <td><button onClick={handleDelete} id={shoe.id} className="btn btn-danger">Delete</button></td>
                    </tr>
                );
                })}
            </tbody>
        </table>
)};

export default ShoeList;