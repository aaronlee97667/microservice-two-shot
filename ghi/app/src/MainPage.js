import { Link } from 'react-router-dom'

function MainPage() {
  return (
    <div className="px-4 py-5 my-5 text-center">
      <h1 className="display-5 fw-bold">WARDROBIFY!</h1>
      <div className="col-lg-6 mx-auto">
        <p className="lead mb-4">
          Need to keep track of your shoes and hats? We have
          the solution for you!
        </p>
        <div className="container">
        <div className="row">
            <div className="col-sm">
              <img style={{ width: 200, height: 200 }} src="https://aesthelook.com/wp-content/uploads/2022/06/34413-gokjpg.jpg"></img>
            </div>
            <div className="col-sm">
              <img style={{ width: 200, height: 200 }} src="https://img.atlasobscura.com/LnEbVA9OaEEMAhUfmN4tPG-BAn5Ye7KqbuHnKwLxE3A/rs:fill:12000:12000/q:81/sm:1/scp:1/ar:1/aHR0cHM6Ly9hdGxh/cy1kZXYuczMuYW1h/em9uYXdzLmNvbS91/cGxvYWRzL2Fzc2V0/cy9lZDgzNjBjMy05/ZDc1LTRiNGMtYWJj/ZS1lYzFkY2Y1NTJl/YjA1YmZiZDA3NDk1/MDNmNzQzOTFfNjEy/MjQgKGMpIE11c2V1/bSBvZiBMb25kb24u/anBn.jpg"></img>
            </div>
          </div>
          <div className="row">
            <div className="col-sm">
              <Link to="/hats" className="btn btn-primary btn-lg px-4 gap-3">Find your Hats</Link>
            </div>
            <div className="col-sm">
              <Link to="/shoes" className="btn btn-primary btn-lg px-4 gap-3">Find your Shoes</Link>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default MainPage;
