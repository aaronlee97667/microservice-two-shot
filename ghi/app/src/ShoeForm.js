import React, { useEffect, useState } from 'react';

function ShoeForm() {
    const [bins, setBins] = useState([])

    const [formData, setFormData] = useState({
        manufacturer: "",
        model_name:"",
        color_name:"",
        picture_url:"",
        bin:""

    })

    const handleNameChange = (e) => {
        setFormData({
            ...formData,
            [e.target.name]: e.target.value,
        })
    }
    const fetchData = async () => {
        const url = 'http://localhost:8100/api/bins/';
        const response = await fetch(url)

        if(response.ok) {
            const data = await response.json()
            setBins(data.bins)
            }
        }
    const handleSubmit = async (event) => {
        event.preventDefault();
        console.log(formData)

        const shoesUrl = 'http://localhost:8080/api/shoes/';
            const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
            };
            const response = await fetch(shoesUrl, fetchConfig);
            if (response.ok) {
                setFormData({
                    manufacturer: "",
                    model_name:"",
                    color_name:"",
                    picture_url:"",
                    bin:""
                })
            }
    };
    useEffect(() => {
        fetchData();
    }, [])

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add a new shoe</h1>
                    <form onSubmit={handleSubmit} id="create-shoes-form">
                       <div className="form-floating mb-3">
                            <input onChange={handleNameChange} value={formData.manufacturer} placeholder="Manufacturer" required type ="text" name="manufacturer" id="manufacturer" className="form-control"></input>
                            <label htmlFor="manufacturer">Manufacturer Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleNameChange} value={formData.model_name} placeholder="Model_name" required type ="text" name="model_name" id="model_name" className="form-control"></input>
                            <label htmlFor="model_name">Model Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleNameChange} value={formData.color_name} placeholder="Color_name" required type ="text" name="color_name" id="color_name" className="form-control"></input>
                            <label htmlFor="color_name">Color Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleNameChange} value={formData.picture_url} placeholder="Picture_url" required type ="text" name="picture_url" id="picture_url" className="form-control"></input>
                            <label htmlFor="picture_url">Picture URL</label>
                        </div>
                            <div className="mb-3">
                                <select onChange={handleNameChange} value={formData.bin} required id="bin" className="form-select" name="bin">
                                <option value="">Choose a bin</option>
                                {bins.map(bin => {
                                            return (
                                            <option key={bin.href} value={bin.href}>
                                                {bin.closet_name}
                                            </option>
                                            );
                                    })}
                                </select>
                            </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )
};

export default ShoeForm;