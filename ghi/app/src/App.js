import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Hats from './HatsList'
import HatForm from './HatForm'
import Nav from './Nav';
import Shoes from './ShoesList'
import ShoeForm from './ShoeForm'

function App(props) {
  if (props.locations === undefined) {
    return null;
  }

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="hats" element={<Hats />} />
          <Route path="newhats" element={<HatForm />} />
          <Route path="shoes" element={<Shoes />} />
          <Route path="newshoes" element={<ShoeForm />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
