import { useState, useEffect } from 'react'
import { Link } from 'react-router-dom'

function HatsList () {

    const [hats, setHats] = useState([])

    const getData = async () => {
        const response = await fetch('http://localhost:8090/api/hats/')
        if (response.ok) {
            const data = await response.json()
            setHats(data.hats)
        }

    }

    useEffect(()=> {
        getData()
    }, [])

    const handleClick = async (e) => {
        const hatresponse = await fetch(`http://localhost:8090/api/hats/${e.target.id}/`)
        if (hatresponse.ok) {
            const hatdetails = await hatresponse.json()
            console.log(hatdetails)
            alert(`The ${hatdetails.color} ${hatdetails.styleName} can be found in:  \n\nCloset Name:  ${hatdetails.location.closet_name}\nSection Number:  ${hatdetails.location.section_number}\nShelf Number:  ${hatdetails.location.shelf_number}`
            )
        }
    }

    const handleDelete = async (e) => {
        console.log(e.target.id)
        const hatsUrl = `http://localhost:8090/api/hats/${e.target.id}/`

        const fetchConfigs = {
            method: "Delete",
            headers: {
                "Content-Type": "application/json"
            }
        }

        const response = await fetch(hatsUrl, fetchConfigs)
        getData()
    }


    return (
        <table className="table table-striped">
        <thead>
            <tr>
            <th>Hat Style</th>
            <th>Color</th>
            <th>Fabric</th>
            <th style={{ display: "flex", alignItems: "center"}}>Image</th>
            </tr>
        </thead>
        <tbody >
            {hats.map(hat => {
            return (
                <tr key={hat.id}>
                <td>{ hat.styleName }</td>
                <td>{ hat.color }</td>
                <td>{ hat.fabric }</td>
                <td><img src={hat.pictureUrl} style={{ width: 200, height: 200 }} /></td>
                <td><button onClick={handleClick} value={hat.location} id={hat.id} className="btn btn-primary">Locate</button></td>
                <td><button onClick={handleDelete} id={hat.id} className="btn btn-danger">Delete</button></td>
                </tr>
            );
            })}
        </tbody>
        </table>
)};

export default HatsList;
