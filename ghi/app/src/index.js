import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

async function loadLocation() {
  const response = await fetch('http://localhost:8100/api/locations/');
  if (response.ok) {
    const data = await response.json();
    root.render(
      <React.StrictMode>
        <App locations={data.locations}/>
      </React.StrictMode>
    )
  } else {
    console.error(response);
  }
}
loadLocation();
