import React, { useEffect, useState } from 'react';

function ConferenceForm () {
    const [locations, setLocations] = useState([])

    const [formData, setFormData] = useState({
        styleName: "",
        color: "",
        fabric: "",
        pictureUrl: "",
        location: "",
    })

    const handleNameChange = (e) => {
        setFormData({
            ...formData,
            [e.target.name]: e.target.value,
        })
    }

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/locations/';
        const response = await fetch(url)

        if(response.ok) {
            const data = await response.json()
            setLocations(data.locations)
            }
        }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const hatsUrl = 'http://localhost:8090/api/hats/';
            const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
            };
            const response = await fetch(hatsUrl, fetchConfig);
            if (response.ok) {
                setFormData({
                    styleName: "",
                    color: "",
                    fabric: "",
                    pictureUrl: "",
                    location: "",
                })

            }
        };

    useEffect(() => {
        fetchData();
      }, []);

    return (
    <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add a new hat</h1>
            <form onSubmit={handleSubmit} id="create-hats-form">
            <div className="form-floating mb-3">
                <input onChange={handleNameChange} value={formData.styleName} placeholder="styleName" required type="text" name="styleName" id="styleName" className="form-control"></input>
                <label htmlFor="styleName">Hat Style</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleNameChange} value={formData.color} placeholder="Color" required type="text" name="color" id="color" className="form-control"></input>
                <label htmlFor="color">Color</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleNameChange} value={formData.fabric} placeholder="Fabric" required type="text" name="fabric" id="fabric" className="form-control"></input>
                <label htmlFor="fabric">Fabric</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleNameChange} value={formData.pictureUrl} placeholder="pictureUrl" required type="text" name="pictureUrl" id="pictureUrl" className="form-control"></input>
                <label htmlFor="pictureUrl">Picture Url</label>
              </div>
                <div className="mb-3">
                    <select onChange={handleNameChange} value={formData.location} required id="location" className="form-select" name="location">
                    <option value="">Choose a location</option>
                    {locations.map(location => {
                                return (
                                <option key={location.href} value={location.href}>
                                    {location.closet_name}
                                </option>
                                );
                        })}
                    </select>
                </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
)};

export default ConferenceForm;
